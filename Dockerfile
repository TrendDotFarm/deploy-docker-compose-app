FROM python:3.13.0
RUN pip install --no-cache-dir ansible==11.0.0
COPY ansible.cfg /etc/ansible/
COPY docker-compose-app-playbook.yml /usr/src/app/
